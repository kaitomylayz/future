from django.shortcuts import render, get_object_or_404, redirect, reverse
from .models import *
from .forms import *

# ДОБАВЛЕНИЕ ПРИЛОЖЕНИЯ С СООБЩЕНИЯМИ
from django.contrib import messages

# Запись (login) пользователя в запрос (request) и удаление (logout) пользователя из запроса
from django.contrib.auth import login, logout

# Декораторы для проверки прав
# login_required - проверка авторизован ли пользователь
# permission_required - проверка прав у авторизированного пользователя
from django.contrib.auth.decorators import login_required, permission_required

# добавить прав к пользователю будет проще всего в админке
# но можно и с помощью кода это сделать

# Стандартная форма для регистрации пользователя и для аутентификации
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

# Собственная форма регистрации и авторизации (см в forms.py)
from .forms import RegistrationForm, LoginForm


# Для создания новых пользовательских прав доступа загляните в models класса Order внутри класса Meta

# Ссылки на страницы регистрации, авторизации и выхода из аккаунта находятся в index.html (../index/)

# Создание пользователя
# Проверка пароля при регистрации проверяется в AUTH_PASSWORD_VALIDATORS

def home(request):
    return render(request, 'magazine/index.html')


def supplier_list(request):
    list_supplier = Supplier.objects.filter(is_exists=True)
    context = {
        'list_sup': list_supplier
    }
    return render(request, 'magazine/supplier/catalog.html', context)


def supplier_detail(request, pk):
    supplier = get_object_or_404(Supplier, pk=pk)

    context = {
        'supplier_object': supplier
    }

    return render(request, 'magazine/supplier/detail.html', context)


# ----------------auth приложение-----------------------------


def anon(request):
    # Проверка
    print('is_active:', request.user.is_active)
    print('is_anonymous:', request.user.is_anonymous)
    print('is_authenticated:', request.user.is_authenticated)
    print('is_staff:', request.user.is_staff)
    print('is_superuser:', request.user.is_superuser)

    # при проверке доступа доступ указывается следующим образом
    # <приложение>.<право>_<модель>
    # Права: add, change, view, delete
    print('может добавлять товар?', request.user.has_perm('magazine.add_product'))
    print('может добавлять и изменять товар?',
          request.user.has_perms(['magazine.change_product', 'magazine.add_product']))
    print('может изменять тип доставки?', request.user.has_perm('magazine.change_delivery_type'))
    return render(request, 'magazine/test/anon.html')


# Проверка на уровне бекенда
# @login_required()

# Если пользователь анонимный, то его перебросит на страницу авторизации (по умолчанию ссылка /accounts/login/)
# Но с помощью login_url можно переопределить
# @login_required(login_url=reverse('log in'))
# Либо в settings сразу переопределить для всего проекта

@login_required()
def auth(request):
    return render(request, 'magazine/test/auth.html')


# Проверка прав доступа
@permission_required('magazine.add_product')
def can_add_product(request):
    return render(request, 'magazine/test/can_add_product.html')


@permission_required(['magazine.add_product', 'magazine.change_product'])
def can_add_change_product(request):
    return render(request, 'magazine/test/can_add_change_product.html')


@permission_required('magazine.change_delivery_type')
def can_change_delivery_type(request):
    return render(request, 'magazine/test/can_change_delivery_type.html')


# ------------------------- 3 pract -----------------------------------------


def user_registration(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)

        if form.is_valid():
            user = form.save()
            print(user)

            login(request, user)

            messages.success(request, 'Вы успешно зарегистрировались')

            return redirect('catalog_product_page')

        messages.error(request, 'Что-то пошло не так')
    else:
        form = RegistrationForm()
    return render(request, 'magazine/auth/registration.html', {'form': form})


def user_login(request):
    if request.method == "POST":
        form = LoginForm(data=request.POST)

        if form.is_valid():
            user = form.get_user()
            login(request, user)

            messages.success(request, 'Вы успешно авторизовались')

            if request.GET.get('next'):
                return redirect(request.GET.get('next'))

            return redirect('catalog_product_page')

        messages.error(request, 'Что-то пошло не так')
    else:
        form = LoginForm()
    return render(request, 'magazine/auth/login.html', {'form': form})


# Деавторизация пользователя
@login_required()
def user_logout(request):
    logout(request)
    messages.warning(request, 'Вы вышли из аккаунта')
    return redirect('log in')


def catalog_product(request):
    many_prod = Product.objects.filter(exists=True)

    # Заполнение формы данными
    if request.GET != None:
        prod_form = ProductFilterForm(request.GET)
    else:
        prod_form = ProductFilterForm()

    # Проверка заполненности данными
    if prod_form.is_valid():

        print(prod_form.cleaned_data)

        if prod_form.cleaned_data.get('name') != "":  # строки имеют "" но не пустоту
            many_prod = many_prod.filter(name__contains=prod_form.cleaned_data.get('name'))

        if prod_form.cleaned_data.get('max_price'):
            many_prod = many_prod.filter(price__lte=prod_form.cleaned_data.get('max_price'))

        if prod_form.cleaned_data.get('min_price'):
            many_prod = many_prod.filter(price__gte=prod_form.cleaned_data.get('min_price'))

    context = {
        'list_object': many_prod,
        'form': prod_form,
    }

    return render(request, 'magazine/product/catalog.html', context)


# Вывод связанных данных с товаром
def product_detail(request, pk):
    product = get_object_or_404(Product, pk=pk)
    context = {
        'product': product
    }
    return render(request, 'magazine/product/detail.html', context)


@permission_required('magazine.add_product')
def product_create(request):
    if request.method == "POST":
        form_prod = ProductForm(request.POST, request.FILES)
        if form_prod.is_valid():
            form_prod.save()
            messages.success(request, 'Продукт успешно добавлен')
            return redirect('catalog_product_page')

        messages.error(request, 'Неверно заполнены поля')
    else:
        form_prod = ProductForm()

    context = {
        'form': form_prod
    }
    return render(request, 'magazine/product/create.html', context)


@login_required()
def buy_product(request, pk):
    prod = Product.objects.get(pk=pk)
    messages.success(request, f'Товар {prod.name} успешно приобретен')
    return redirect('catalog_product_page')


@permission_required('magazine.add_supplier')
def supplier_create(request):
    if request.method == "POST":
        form_supplier = SupplierForm(request.POST)
        if form_supplier.is_valid():
            form_supplier.save()

            messages.success(request, 'Товар успешно добавлен')

            return redirect('catalog_product_page')

        messages.error(request, 'Неверно заполнены поля')
    else:
        form_supplier = SupplierForm()

    context = {
        'form': form_supplier
    }
    return render(request, 'magazine/supplier/create.html', context)


# --------------------------------------- View generic ----------------------
# Набор стандартных CRUD действий
# ListView - Список объектов
# DetailView - Полная информация выбранного объекта
# CreateView - Создать
# UpdateView - Изменить
# DeleteView - Удалить

from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

# Для того чтобы добавить проверку прав доступа, надо будет воспользоваться method_decorator
from django.utils.decorators import method_decorator

#
from django.urls import reverse, reverse_lazy


class CategoryList(ListView):  # Возврат листа объектов (Категорий)

    # Определяем модель для получения данных
    model = Category
    # Установка собственного шаблона
    # (по умолчанию 'magazine/category_list.html')
    # (по умолчанию '<название приложения>/<название_модели>_list.html>')
    template_name = 'magazine/category/category_list.html'
    # Изменение ключа для передачи данных (object_list)
    # context_object_name = 'book_list'

    # Доп значения (вторичные/статичные данные)
    extra_context = {
        'title': 'Список книг из класса'
    }

    # Вывод страницы если ни один объект не был найден
    # True - выводит страницу с пустым списком
    # False - выводит ошибку 404
    allow_empty = True

    # Подключение пагинации
    # С помощью числа выбираем сколько объектов будем выводить на страницу
    paginate_by = 1

    # Если надо подключить пагинатор для метода
    # from django.core.paginator import Paginator

    # Доп значение (Динамичные/обрабатываемые данные)
    # Переопределение метода для добавления доп. данных
    # К примеру, сюда можно подставить обработку фильтрации
    def get_context_data(self, *, object_list=None, **kwargs):
        # Получение имеющихся данных
        context = super().get_context_data(**kwargs)

        # #
        # context['title'] = 'Список книг из класса (полученные внутри метода get_context_data)'
        #
        # #
        # context['count_pub'] = publishing_house.objects.all().count()
        #
        # # Получение категорий
        # context['categories'] = category.objects.all()
        return context

    # Переопределение основного запроса для получения списка объектов
    def get_queryset(self):
        return Category.objects.all()  # Запрос по умолчанию


class CategoryDetail(DetailView):
    model = Category

    # (по умолчанию 'magazine/category_detail.html')
    # (по умолчанию '<название приложения>/<название_модели>_detail.html>')
    template_name = 'magazine/category/category_detail.html'

    # (по умолчанию 'object')
    # context_object_name = 'category'

    # Переопределение получаемого параметра
    # pk_url_kwarg = 'category_id'

    def get_context_data(self, *, object_list=None, **kwargs):  # Переопределение метода для добавления доп. данных
        context = super().get_context_data(**kwargs)
        # Получение категорий из объекта книги
        # context['products'] = context['pk'].product_set.all()
        return context


# Создание, Изменение, Удаление примечательно тем,
# что мы можем обратиться к нему как через GET так и через POST
# В случае GET - Открываем страницу
# В случае POST - Выполняем операцию
class CategoryCreate(CreateView):
    model = Category

    # Форма, которая будет использоваться
    # Форма передается с ключом form
    form_class = CategoryForm

    extra_context = {
        'action': 'Создать',
    }

    # (по умолчанию 'magazine/category_form.html')
    # (по умолчанию '<название приложения>/<название_модели>_form.html>')
    template_name = 'magazine/category/category_form.html'

    # Путь переадресации при успешном добавлении
    # По умолчанию пытается обратиться к get_absolute_url() (см в models.py class Category)
    # success_url = reverse_lazy('category_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    # С помощью декоратора навешиваем декоратор на метод
    # Метод dispatch нужен для определения метода запроса (GET, POST...)
    # и соответственно это первый вызываемый метод, чтобы сразу проверить
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class CategoryUpdate(UpdateView):
    model = Category
    form_class = CategoryForm

    extra_context = {
        'action': 'Изменить',
    }

    # (по умолчанию 'magazine/category_form.html') также как и при добавлении
    # (по умолчанию '<название приложения>/<название_модели>_form.html>')template_name = 'book/books/books-update.html'
    template_name = 'magazine/category/category_form.html'

    # pk_url_kwarg = 'category_id'

    # context_object_name = 'category'

    @method_decorator(permission_required('magazine.change_books'))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class CategoryDelete(DeleteView):
    model = Category

    # (по умолчанию 'magazine/category_form.html') также как и при добавлении
    # (по умолчанию '<название приложения>/<название_модели>_form.html>')template_name = 'book/books/books-update.html'
    template_name = 'magazine/category/category_confirm_delete.html'
    success_url = reverse_lazy('category_list')

    @method_decorator(permission_required('book.delete_books'))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


# -------------------------------- Mixin -------------------------------

# Пример использования миксина в классе
from .utils import CalculateMoney


class OrderDetail(DetailView, CalculateMoney):
    model = Order

    template_name = 'magazine/order/order_detail.html'

    def get_context_data(self, *, object_list=None, **kwargs):  # Переопределение метода для добавления доп. данных
        context = super().get_context_data(**kwargs)

        order = context.get('object')

        list_price = [pos_order.sum_pos_order() for pos_order in order.pos_order_set.all()]
        # Обращаемся к методу из CalculateMoney
        context['sum_price'] = self.sum_price(prices=list_price)
        return context


# --------------------------------- Email ---------------------------

# Email
from django.core.mail import send_mail, send_mass_mail
from django.conf import settings


# EMAIL

def contact_email(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            mail = send_mail(
                form.cleaned_data['subject'],
                form.cleaned_data['content'],
                settings.EMAIL_HOST_USER,
                ['mail_for_django@mail.ru'],
                fail_silently=True
            )
            if mail:
                messages.success(request, 'Письмо успешно отправлено.')
                return redirect('book_list_class')
            else:
                messages.error(request, 'Письмо не удалось отправить, попробуйте позже.')
        else:
            messages.warning(request, 'Письмо неверно заполнено, перепроверьте внесенные данные.')
    else:
        form = ContactForm()
    return render(request, 'book/email.html', {'form': form})


# ------------------------------------- API --------------------------

# Для начала скачиваем пакет django_rest_framework
# pip install django_rest_framework
# Т.к. это приложение, его необходимо зарегистрировать в settings.py
#    'rest_framework',  # Установка REST API

# Теперь мы можем воспользоваться различными методами и классами

# API
# JsonResponse отправляет данные в JSON формате (для этого данные должны быть словарем)
from django.http import JsonResponse
from .seializers import *

# status - пакет со всеми статусными кодами для настройки ответа
from rest_framework import status

# Response - функция для отправки данных
# если использовать именно её то в стандартные формы страницы она будет отображаьб
from rest_framework.response import Response
# api_view - декоратор, внутри него можно описать доступные методы
# также он возвращает стандартную страницу документации
from rest_framework.decorators import api_view
# viewsets - generic класс, с CRUD операциями
from rest_framework import viewsets


# API можно даже не добавляя приложение django_rest_framework
# По сути, любой сайт можно легко переделать в API изменив возвращаемое значение
# Также как и API в сайт
# Для этого создадим простой метод, который возвращает JSON
# JSON опишем в виде словаря

def test_json(request):
    return JsonResponse({
        'message': 'Данный в виде JSON',
        'api_test': reverse_lazy('api_test'),
        'order_api_list': reverse_lazy('api_order_list'),
        'order_api_detail': reverse_lazy('api_order_detail'),
        'products_api_viewset': '/mag/api/products/',
    })


# Указываем какие методы доступны
# С помощью format - указывается формат вывода по умолчанию
# (http - справочная страница, json - возврат данных Json)
@api_view(['GET', 'POST'])
def order_api_list(request, format=None):
    # Проверяем запрос
    if request.method == 'GET':

        # Получаем данные из БД
        # book_list = books.objects.all()
        order_list = Order.objects.filter(exists=True)

        # Преобразуем данные в словарь с помощью сериализатора
        # По умолчанию сериализатор работает с одним объектом
        # но если у нас список объектов, то следует включить параметр many
        serializer = OrderSerializer(order_list, many=True)

        # Вывод данных
        print(serializer.data)

        # Возврат значений, можно через JsonResponse, тогда django будет возвращать данные
        # return JsonResponse(serializer.data, safe=False)
        # return JsonResponse({'books': serializer.data})

        # Либо через Response, чтобы приложение rest_framework могло не только возвращать данные,
        # но и показывать справочную страницу
        return Response({'orders': serializer.data})
    elif request.method == 'POST':  # Добавляем объект в БД

        # В этом случае данные мы получаем, а не преобразуем
        serializer = OrderSerializer(data=request.data)

        # Проверка полученных данных
        if serializer.is_valid():
            # Сохранение
            serializer.save()
            # Вывод добавленных данных (необязательно) и указание статусного кода
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        # Вывод в случае ошибки
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def order_api_detail(request, pk, format=None):
    order_obj = get_object_or_404(Order, pk=pk)
    if order_obj:
        if request.method == 'GET':
            # Получение одного объекта
            serializer = OrderSerializer(order_obj)
            return Response(serializer.data)
        elif request.method == 'PUT':
            serializer = OrderSerializer(order_obj, data=request.data)
            if serializer.is_valid():
                serializer.save()

                return Response({'message': 'Данные успешно обновлены', 'order': serializer.data})

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        elif request.method == 'DELETE':
            # Удаление объекта
            order_obj.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        # Если объект не был найден
        return Response(status=status.HTTP_404_NOT_FOUND)


# Также можно воспользоваться viewsets
# По сути является набором generic View и внутри имеет все CRUD операции
# Но загвоздка в маршрутизации, т.к. список и добавление имеют один маршрут
# а вывод одного объекта, изменение и удаление другой маршрут, внутри таких наборов
# есть свой роутер с маршрутами, который нужно отдельно регистрировать в urls.py

# Следующая часть объяснения подключения маршрутизации лежит в magazine/urls.py
class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.filter(exists=True)
    serializer_class = ProductSerializer


# ------------------------------------- handler -------------------------------

# Переопределение страниц с ошибками

# Создаем отдельный метод с указанием страницы и добавляем к рендеру статусный код,
# далее надо будет указать этот метод в соответствующий handler в маршрутизации ПРОЕКТА (не приложения)
# Переопределние ошибок происходит в Code_future/urls.py
def error_404(request, exception):
    return render(
        request,
        'magazine/error/404.html',
        {
            'title': 'Страница не найдена',
            'exception': exception
        },
        status=404,
    )
