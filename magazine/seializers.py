# API
# Сериализаторы нужны для преобразований одного вида данных в другие виды
# В нашем случае из QuerySet, object в словарь
from rest_framework import serializers
from .models import Product, Order

# Как и forms, models сериализаторы тоже использую схожую структуру с Fields или Model<название>
# Поэтому тут все схоже что и с формочками
class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        # fields = '__all__'
        fields = [
            'pk',
            'FIO_customer',
            'delivery_address',
            'delivery_type',
            'date_create',
            'date_finish',
            'books',
        ]

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'name',
            'description',
            'price',
            'create_date',
            'update_date',
            'photo',
            'exists',
            'category',
            'tag',
            'parametr',
        ]


# Можно построить сериализатор и без модели для того чтобы преобразовывать данные
class EmailSerializer(serializers.Serializer):
    recipient = serializers.EmailField()
    subject = serializers.CharField()
    content = serializers.CharField()
