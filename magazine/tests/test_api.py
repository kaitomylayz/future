from rest_framework.test import APITestCase
from django.urls import reverse
from magazine.models import Product, Category
from magazine.seializers import ProductSerializer
from rest_framework import status

# Следует учитывать что при тестировании объекты БД создаются в локальной виртуальной БД
# Тем самым саму БД мы не трогаем и можем безопасно тестировать соответствие

class ProductAPITestCase(APITestCase):
    def test_get_list(self):
        category_1 = Category.objects.create(name='Категория')
        product_1 = Product.objects.create(name='Test1', price=194.5, category=category_1)
        product_2 = Product.objects.create(name='Test2', price=223.78, category=category_1, description='Книга для тестирования')
        url = '/mag/api/products/'

        # print('url:', url)
        # print('category_1:', category_1)
        # print('product_1:', product_1)
        # print('product_2:', product_2)

        response = self.client.get(url)

        serial_data = ProductSerializer([product_1, product_2], many=True).data

        # print('serial_data:', serial_data)
        # print('response:', response.data)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serial_data, response.data)

    def test_post_list(self):
        # Проверка со стороны тест-кейс
        category_1 = Category.objects.create(name='Категория')
        product_1 = Product(pk=3, name='Test1', price=374.12, category=category_1)
        serial_data = ProductSerializer(product_1).data

        # print(product_1)
        # print(category_1)
        # print(serial_data)

        # Проверка со стороны проекта (views.book_api_list)
        url = '/mag/api/products/'
        response = self.client.post(url, data={
            'name': 'Test1',
            'price': 374.12,
            'category': category_1.pk,
            'exists': True,
        })

        # print(url)
        # print(product_1)

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(serial_data.get('name'), response.data.get('name'))
        self.assertEqual(serial_data.get('description'), response.data.get('description'))
        self.assertEqual(serial_data.get('price'), response.data.get('price'))
        self.assertEqual(serial_data.get('category'), response.data.get('category'))
        self.assertEqual(serial_data.get('exists'), response.data.get('exists'))

