from django.contrib import admin
from .models import *


@admin.register(Supplier)
class SupplierAdmin(admin.ModelAdmin):
    list_display = ('name', 'FIO_agent', 'agent_telephone')
    list_display_links = ('name',)
    search_fields = ('name', 'FIO_agent',)
    list_editable = ('agent_telephone',)
    # list_filter = ()
    ordering = ('name',)

    # Изменение отображение полей в админке из составных полей
    @admin.display(description="ФИО представителя")
    def FIO_agent(self, obj):
        if obj.agent_patronymic:
            return f"{obj.agent_firstname} {obj.agent_name[0]}. {obj.agent_patronymic[0]}."
        return f"{obj.agent_firstname} {obj.agent_name[0]}."


@admin.register(Supply)
class SupplyAdmin(admin.ModelAdmin):
    list_display = ('id', 'date_supply', 'supplier_name')
    list_display_links = ('id', 'date_supply')
    list_filter = ('supplier__name',)
    ordering = ('-date_supply',)

    @admin.display(description="Поставщик")
    def supplier_name(self, obj):
        return obj.supplier.name


@admin.register(Pos_supply)
class Pos_supplyAdmin(admin.ModelAdmin):
    list_display = ('id', 'product', 'supply', 'count')
    list_display_links = ('id',)
    search_fields = ('product__name', 'supply__supplier__name')
    list_editable = ('count',)
    list_filter = ('supply__supplier__name',)
    ordering = ('-supply__id',)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    ordering = ('name',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    ordering = ('name',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('FIO_customer', 'delivery_address', 'delivery_type', 'date_create', 'date_finish')
    list_display_links = ('FIO_customer',)
    search_fields = ('FIO_customer', 'delivery_address')
    list_editable = ('date_finish',)
    list_filter = ('delivery_type',)
    ordering = ('-date_create',)



@admin.register(Pos_order)
class Pos_orderAdmin(admin.ModelAdmin):
    list_display = ('order', 'product', 'count', 'discount')
    list_display_links = None
    search_fields = ('product__name', 'order__id')
    list_editable = ('product', 'count', 'discount')
    list_filter = ('order',)
    ordering = ('-order__id',)


@admin.register(Parametr)
class ParametrAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    ordering = ('name',)


@admin.register(Pos_parametr)
class Pos_parametrAdmin(admin.ModelAdmin):
    list_display = ('product', 'parametr', 'value')
    list_display_links = None
    search_fields = ('product__name', 'parametr__name')
    list_editable = ('product', 'parametr', 'value')
    list_filter = ('product', 'parametr')
    ordering = ('-product__name',)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'exists', 'category')
    search_fields = ('name', 'description')
    list_editable = ('price', 'exists')
    list_filter = ('exists', 'category')
    ordering = ('-create_date',)
