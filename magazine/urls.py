from django.urls import path
from .views import *

urlpatterns = [
    # name - это установка названия обратной ссылки (названия должны быть уникальны)
    # таким образом, можно пользоваться не ссылками, а названиями
    # что проще, т.к. URL может меняться, а название нет и по названию
    # можно подставлять URL

    # Для указания обратной ссылки нужно в html там где должна быть ссылки прописать
    # Тэг Django {% url '<название пути>' %}
    # Пример:  <a href="{% url 'catalog_product_page' %}">Каталог</a>
    path('index/', home, name='home_page'),

    path('catalog/', catalog_product, name='catalog_product_page'),

    # Если нужно указать передаваемый параметр его можно указать после названия
    # Пример: <a href="{% url 'product_detail_page' 1 %}">Подробнее</a>
    # Пример: <a href="{% url 'product_detail_page' pk=1 %}">Подробнее</a>
    # Пример: <a href="{% url 'product_detail_page' object.pk %}">Подробнее</a>
    path('product/<int:pk>/', product_detail, name='product_detail_page'),
    path('product/create/', product_create, name='create_product_page'),

    path('product/buy/<int:pk>/', buy_product, name='buy_product_page'),

    path('suppliers/', supplier_list, name='catalog_suppliers_page'),
    path('suppliers/create/', supplier_create, name='create_suppliers_page'),
    path('suppliers/detail/<int:pk>/', supplier_detail, name='detail_suppliers_page'),

    # -------------------------- auth ----------------------------------
    path('registration/', user_registration, name='regis'),
    path('login/', user_login, name='log in'),
    path('logout/', user_logout, name='log out'),


    path('anon/', anon, name='anon'),
    path('auth/', auth, name='auth'),
    path('can_add/', can_add_product, name='add'),
    path('can_add_change/', can_add_change_product, name='add_change'),
    path('can_change_del/', can_change_delivery_type, name='change_type'),

    # --------------------------- ClassView ------------------------------
    path('category/', CategoryList.as_view(), name='category_list'),
    path('category/create', CategoryCreate.as_view(), name='category_create'),
    path('category/<int:pk>/detail/', CategoryDetail.as_view(), name='category_detail'),
    path('category/<int:pk>/update/', CategoryUpdate.as_view(), name='category_update'),
    path('category/<int:pk>/delete/', CategoryDelete.as_view(), name='category_delete'),

    # ----------------------------- Email ------------------------

    path('contact/', contact_email, name='email_page'),

    # ----------------------------- API ---------------------

    path('api/', test_json, name='api_test'),
    path('api/orders/', order_api_list, name='api_order_list'),
    path('api/orders/<int:pk>/', order_api_detail, name='api_order_detail'),
]

# ----------------------- API View --------------------
# Для добавления путей API нужно сначала создать отдельный роутер,
# куда внесем наши пути, потом эти пути добавим в urlpatterns

from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'api/products', ProductViewSet, basename='product')

urlpatterns += router.urls