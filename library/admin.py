# Админка нужна для работы с БД, чтобы не приходилось работать с
# интерфейсами СУБД. Тем самым облегчаю работу
from django.contrib import admin
from .models import *

# Для доступа к админки сначала следует создать пользователя,
# через которого сможем зайти на приложения admin

# Для появления модели в админки необходимо её зарегистрироваться
admin.site.register(Pos_order)


# Также Зарегистрировать модель можно следующим образом
@admin.register(Publishing_house)
class Publishing_houseAdmin(admin.ModelAdmin):
    pass


# К тому же можно создать класс для настройки вывода и работы с моделью в таблице
class OrderAdmin(admin.ModelAdmin):
    list_display = ('pk', 'address_delivery', 'date_create', 'date_finish')  # Отображение полей
    list_display_links = ('address_delivery',)  # Установка ссылок на атрибуты
    search_fields = ('address_delivery',)  # Поиск по полям
    list_editable = ('date_finish',)  # Изменяемые поля


# Соответственно, в этом случае регистрируем модель и её отображение
admin.site.register(Order, OrderAdmin)


# Конечно можно сделать это через декоратор, указав модель и прописав класс
# По сути, в декораторе происходит тоже самое что и выше, а именно регистрация
# модели и её отображения (CRTL+ЛКМ по register)
@admin.register(Books)
class BooksAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'price', 'exists')  # Отображение полей
    list_display_links = ('name',)  # Установка ссылок на атрибуты
    search_fields = ('name', 'price')  # Поиск по полям
    list_editable = ('price', 'exists')  # Изменяемые поля
    list_filter = ('exists',)  # Фильтры полей


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass

#  Название атрибутов прописывается в самих атрибутах (models)
#  Название записи прописывается в __str__ (models)
#  Название модели и сортировка её атрибутов прописывается в классе Meta (models)
#  Отображение списка записей в админке прописывается в модуле админки приложения (admin)
#  Название приложения прописывается в настройках приложения (apps)
