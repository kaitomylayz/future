from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
# from django.db.models import Q, Avg, Min, Max

# from . import models as m

# ПОМЕНЯЛ ИМПОРТ МОДЕЛЕЙ, ТЕПЕРЬ ИМПОРТИРУЮ НА ПРЯМУЮ
from .models import *


def home(request):
    return HttpResponse("<h1>Домашняя страница</h1>")


def all_books(request):
    books = Books.objects.all()
    page = "<h1>Книги:</h1>"

    for book in books:
        page += f"<p>{book.name}</p>"
    return HttpResponse(page)


def id_book(request, id):
    book = get_object_or_404(Books, pk=id)

    page = "<h1>Книга:</h1>"
    page += f"<p>{book.name}</p>"
    page += f"<p>{book.description}</p>"
    page += f"<p>{book.price}</p>"
    page += f"<p>{book.count_pages}</p>"
    page += f"<p>{book.exists}</p>"
    return HttpResponse(page)


def name_book(request, name):
    books = Books.objects.filter(name__contains=name)
    page = "<h1>Книги по имени:</h1>"

    for book in books:
        page += f"<p>{book.name}</p>"
    return HttpResponse(page)


# ------------------NEW--------------------------

def info(request):
    # С помощью метода render можно вызывать страницы из HTML.

    # Первым параметром пишем request (запрос), таким образом передадим запрос на страницу + серверу

    # Вторым параметром идет сама html
    # Точка начала берется из папки templates (надо создать самостоятельно)
    # внутри приложения (Code_future/library/templates) (в settings.py это указано 'APP_DIRS': True) соответственно,
    # если страница лежит по пути Code_future/library/templates/index.html достаточно прописать index.html,
    # но следует создать также папку library (по названию приложения), т.к. механизм DjangoTemplate работает особенным
    # образом, об этом будет говориться в 4 модуле, когда будем разбирать компонент Templates из MVT

    # имеется и третий параметр, для передачи данных на страницу, об этом будет ниже
    return render(request, 'library/index.html')


def get_all_book(request):
    books = Books.objects.all()

    # Третий параметр это context
    # С помощью его передаются данные на страницу html
    # Передаваемые значение должны быть в виде словаря, таким образом задается <имя_переменной>:<значение переменной>
    context = {
        'list': books  # list - ключ, переменная в .html, books - само значение
        # Ключ list (view) == переменной {{ list }} (template)
    }
    return render(request, 'library/query_all.html', context=context)


# После написания методов не забудьте добавить их в маршрутизацию urls.py

def get_one_book(request, id):
    book = get_object_or_404(Books, pk=id)
    return render(request, 'library/query_get.html', {'one_book': book})


def get_one_filter_book(request):
    # К тому же мы можем принимать параметры (то что прописывается после ? в адресе (?price=200))
    # Однако параметры лежать в request, при этом параметры рассортированы, в зависимости от переданного параметра HTTP
    # Если данные передавались через GET, то лежать будут в request.GET
    # Если данные передавались через POST, то лежать будут в request.POST
    # Данные передаются в виде словаря, поэтому можно получать по ключу (см. index.html)

    find_books = Books.objects.filter(exists=request.GET.get('is_ex'))
    return render(request, 'library/query_filter.html', {'filter_books': find_books})


def get_more_filter_book(request):
    # мы можем передавать больше фильтров, так к тому же и разнообразнее
    # к тому же фильтры можно комбинировать с условиями, для этого после атрибута пишем условие
    # Пример: price__lt - фильтр lt это lesser than (меньше чем <) и после можем задать значение
    # Пример: price__lt=200 - значит нам будут возвращаться книги чья цена меньше 200
    # Список фильтров: (https://django.fun/docs/django/4.2/ref/models/querysets/#field-lookups)
    # exact - Точное совпадение
    # contains - LIKE (ищет подстроку в строке)
    # in - ищет значения из представленного списка
    # gt - >
    # gte - >=
    # lt - <
    # lte - <=
    # startswith - начало строки с указанной подстроки
    # endswith - завершения строки с указанной подстроки
    # остальное найдете по ссылке

    find_books = Books.objects.filter(
        price__lte=request.GET.get('max_price'),
        price__gt=request.GET.get('min_price')  # задаем диапазон цен (ориентируйтесь по фильтрам, ключи могут быть)
        # какими угодно.    price <=(lte) max_price, price >(gt) min_price
    )  # В этом случае условия буду складываться, т.е. будет использоваться приставка AND
    return render(request, 'library/query_filter.html', {'filter_books': find_books})


def get_one_by_one_filter_book(request):
    # мы можем передавать больше фильтров поочередно

    find_books = Books.objects.filter(price__lte=request.GET.get('max_price'))
    find_books = find_books.filter(name__contains=request.GET.get('name'))

    # Books.objects.filter(Q(price__lt=300) | Q(price__gt=500))

    # Запросы "ленивые", т.е. они выполняются лишь по требованию, и мы можем сколь угодно добавлять значения
    # при этом к БД не отправляться множество запросов с добавлением новых фильтров

    return render(request, 'library/query_filter.html', {'filter_books': find_books})


def create_blank_book(request):
    # Для создания книги необходимо создать объект из класса
    new_book = Books()

    # При появлении ошибки раскомментируйте строку ниже
    # new_book.price = 100
    # Ошибка возникает из-за пустого значения не в пустом поле, так-то если в атрибуте прописано автоматическое или
    # значение по умолчанию оно и будет подставляться, в ином, будет подставлять NULL


    # С помощью метода save сохраняем объект, если у объекта пустое значение (null) в pk, то такой объект создаться
    # если у объекта имеется значение в pk, то такой объект будет обновлен
    new_book.save()
    return render(request, 'library/message.html', context={'message': 'пустая книга создана'})


def create_book(request):
    # Значение можно передать как в конструктор класс Books, так и добавить по мере продвижения по коду

    # new_book = Books(name='Мега книга', price=256, count_pages=128)
    # ==
    new_book = Books()
    new_book.name = 'Мега книга'
    new_book.price = 256
    new_book.count_pages = 128

    new_book.save()
    return render(request, 'library/message.html', context={'message': 'книга с данными создана'})


def update_book(request, pk):
    upd_book = get_object_or_404(Books, pk=pk)

    upd_book.price = 1000

    upd_book.save()
    return render(request, 'library/message.html', context={'message': 'Цена книги изменена на 1000'})

    # Запускаем python console (слева снизу в Pycharm) и можем прописывать команды ниже (Тестировать в реальном времени)

    # from library.models import *

    # book_one = Books(name='451 градус по Фаренгейту',price=160)
    # book_one.save()

    # book_two = Books()
    # book_two.name = 'Евгений Онегин'
    # book_two.price = 215
    # book_two.save()

    # Books.objects.create(name='Басни',price=90)
    # book_three = Books.objects.create(name='Басни',price=90)

    # Вывод записей

    # Books.objects.all()
    # list_Books = Books.objects.all()
    # list_Books[1].name

    # book_from_db = Books.objects.get(pk=3)

    # Изменения данных

    # book_from_db = Books.objects.get(pk=3)
    # book_from_db.name
    # book_from_db.price = 190
    # book_from_db.save()

    # Удаление данных

    # book_from_db = Books.objects.get(pk=4)
    # book_from_db.delete()

    # Сортировка данных

    # Books.objects.order_by('name') # ASC
    # Books.objects.order_by('-name') # DESC

    # Books.objects.order_by('name').first() # Вывод первой записи
    # Books.objects.all().first()

    # Books.objects.order_by('name').last() # Вывод последней записи
    # Books.objects.all().last()

    # Books.objects.earliest('create_date') # Ранняя дата
    # Books.objects.latest('create_date') # Поздняя дата

    # Books.objects.filter(category__title='Повесть')
    # <QuerySet [<Books: Над пропастью во ржи>, <Books: Собачье сердце>, <Books: Тарас бульба>]>
    # Books.objects.exclude(category__title='Повесть')
    # <QuerySet [<Books: Алиса в Стране чудес>, <Books: Анна Каренина>, <Books: Маленький принц>, <Books: Фауст>]>
    #
    #
    # Books.objects.filter(count_pages__gt=300).aggregate(Min('price'))
    # {'price__min': 349.0}
    # Books.objects.filter(count_pages__gt=300).aggregate(Max('price'))
    # {'price__max': 843.0}
    # Books.objects.filter(count_pages__gt=300).aggregate(Avg('price'))
    # {'price__avg': 572.75}
    #
    #
    # Books.objects.aggregate(Min('price'))
    # {'price__min': 200.0}
    # Books.objects.aggregate(Max('price'))
    # {'price__max': 843.0}
    # Books.objects.aggregate(Avg('price'))
    # {'price__avg': 452.57142857142856}
    # Books.objects.aggregate(Count('price'))
    # {'price__count': 7}
    # Books.objects.aggregate(Sum('price'))
    # {'price__sum': 3168.0}
    #
    #
    #
    #
    # book = Books.objects.filter(count_pages__gt=300)
    # book.update(count_pages=F('count_pages')+1)
    #
    #
    #
    # Books.objects.filter(count_pages__gt=300).values('name','price')
    #
    # Books.objects.filter(count_pages__gt=300).values('name')
    # <QuerySet [{'name': 'Алиса в Стране чудес'}, {'name': 'Анна Каренина'}, {'name': 'Собачье сердце'}, {'name': 'Фауст'}]>
    # Books.objects.filter(count_pages__gt=300).values('price')
    # <QuerySet [{'price': 843.0}, {'price': 693.0}, {'price': 406.0}, {'price': 349.0}]>
    #
    #
    #
    # Books.objects.filter(category__title='Роман')
    # <QuerySet [<Books: Анна Каренина>, <Books: Над пропастью во ржи>]>
    # Books.objects.filter(category__title='Повесть')
    # <QuerySet [<Books: Над пропастью во ржи>, <Books: Собачье сердце>, <Books: Тарас бульба>]>
    #
    # Books.objects.filter(category__title__in=['Повесть','Роман'])
    # <QuerySet [<Books: Анна Каренина>, <Books: Над пропастью во ржи>, <Books: Над пропастью во ржи>, <Books: Собачье сердце>, <Books: Тарас бульба>]>
    #
    # Books.objects.filter(Q(price__lt=300) | Q(price__gt=500))
    # <QuerySet [<Books: Алиса в Стране чудес>, <Books: Анна Каренина>, <Books: Маленький принц>, <Books: Над пропастью во ржи>]>
    #
    # Books.objects.filter(price__gt=300, price__lt=500)
    # <QuerySet [<Books: Собачье сердце>, <Books: Тарас бульба>, <Books: Фауст>]>
