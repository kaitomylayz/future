# Файл маршрутизации приложения
# Изначально его нет, но его можно создать

from django.urls import path
from . import views as v

# Здесь прописываются пути самого приложения с добавлением уровня в 'Code_future.urls'
# т.е. если здесь указан путь 'index/', то в адресе следует прописать полный путь
# берущий с самого начала 'lib/index/'

# Также в конце каждой ссылки (кроме пустых) следует добавлять '/',
# чтобы при подключении или добавлении новых ссылок не было слипания
# (по типу libindex/)

urlpatterns = [
    # path('', v.home),
    path('index/', v.home),
    path('books/', v.all_books),

    # Можно передавать параметры через путь
    # Сначала указывается тип данных, потом названия ключа
    # Главное чтобы ключ совпадал с параметром внутри метода
    # <int:id> == def id_book(request,id):
    path('books/<int:id>/', v.id_book),

    # Учитывайте порядок путей, т.к. чем выше, тем приоритетнее,
    # а значит, выше явное, ниже неявное
    path('books/<str:name>/', v.all_books),


    path('', v.info),
    path('query/books/', v.get_all_book),
    path('query/books/<int:id>/', v.get_one_book),
    path('query/books/one-filter/', v.get_one_filter_book),
    path('query/books/more-filter/', v.get_more_filter_book),
    path('query/books/one-by-one-filter/', v.get_one_by_one_filter_book),
    path('query/books/create_blank/', v.create_blank_book),
    path('query/books/create/', v.create_book),
    path('query/books/update/<int:pk>/', v.update_book),
]


