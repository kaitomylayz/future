from django.db import models


class Books(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название книги')
    description = models.TextField(blank=True, null=True, verbose_name='Описание')
    count_pages = models.IntegerField(null=True, verbose_name='Количество страниц')
    price = models.FloatField(verbose_name='Цена')
    release_date = models.DateField(auto_now_add=True, verbose_name='Дата издания')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания записи')
    update_date = models.DateTimeField(auto_now=True, verbose_name='Дата последнего обновления записи')
    photo = models.ImageField(upload_to='image/%Y/%m/%d', null=True, verbose_name='Фотография книги')
    exists = models.BooleanField(default=True, verbose_name='Существует ли?')

    publisher = models.ForeignKey('Publishing_house', on_delete=models.PROTECT, null=True, verbose_name='Издатель')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'
        ordering = ['name', '-price']


class Publishing_house(models.Model):
    title = models.CharField(max_length=150, verbose_name='Название')
    agent_firstname = models.CharField(max_length=50, verbose_name='Фамилия представителя')
    agent_name = models.CharField(max_length=50, verbose_name='Имя представителя')
    agent_patronymic = models.CharField(max_length=50, null=True, verbose_name='Отчество представителя')
    agent_telephone = models.CharField(max_length=50, null=True, verbose_name='Телефон представителя')

    def __str__(self):
        return self.title + " " + self.agent_firstname + ":" + self.agent_telephone

    class Meta:
        verbose_name = 'Издатель'
        verbose_name_plural = 'Издатели'


class Category(models.Model):
    title = models.CharField(max_length=100, verbose_name='Название')
    description = models.TextField(verbose_name='Описание')


    books = models.ManyToManyField(Books)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Order(models.Model):
    date_create = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания заказа')
    date_finish = models.DateTimeField(null=True, blank=True, verbose_name='Дата завершения заказа')  # ( ,"")
    price = models.FloatField(null=True, blank=True, verbose_name='Цена заказа')
    address_delivery = models.CharField(max_length=255, verbose_name='Адрес доставки')


    books = models.ManyToManyField(Books, through='Pos_order')

    def __str__(self):
        return str(self.date_create) + " " + str(self.price) + " " + self.address_delivery

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ['date_create']


class Pos_order(models.Model):
    book = models.ForeignKey(Books, on_delete=models.CASCADE, verbose_name='Книга')
    order = models.ForeignKey(Order, on_delete=models.CASCADE, verbose_name='Заказ')
    count_books = models.IntegerField(verbose_name='Количество книг')

    def __str__(self):
        return self.order.__str__() + " " + self.book.__str__() + "|" + str(self.count_books)

    class Meta:
        verbose_name = 'Позиция заказа'
        verbose_name_plural = 'Позиции заказов'
        ordering = ['order']



class Passport_book(models.Model):
    article = models.IntegerField(verbose_name='Артикль')
    features = models.CharField(max_length=255, verbose_name='Свойства книги')

    book = models.OneToOneField(Books, on_delete=models.PROTECT, primary_key=True, verbose_name='Книга')

    def __str__(self):
        return str(self.article) + " | " + self.book.__str__()

    class Meta:
        verbose_name = 'Паспорт книги'
        verbose_name_plural = 'Паспорт книги'

# Пустышка (чтобы комменты ниже были читаемы)
""

# После написания всех таблиц необходимо создать миграцию
# python manage.py makemigrations
# Как только появится миграция можно её провести с помощью:
# python manage.py migrate

# При любых изменениях следует создавать и применять миграции



# Команда для запуска консоли - python manage.py shell

# Сохранение записи

# book_one = books(name='451 градус по Фаренгейту',price=160)
# book_one.save()

# book_two = books()
# book_two.name = 'Евгений Онегин'
# book_two.price = 215
# book_two.save()

# books.objects.create(name='Басни',price=90)
# book_three = books.objects.create(name='Басни',price=90)

# Вывод записей

# books.objects.all()
# list_books = books.objects.all()
# list_books[1].name

# book_from_db = books.objects.get(pk=3)

# Изменения данных

# book_from_db = books.objects.get(pk=3)
# book_from_db.name
# book_from_db.price = 190
# book_from_db.save()

# Удаление данных

# book_from_db = books.objects.get(pk=4)
# book_from_db.delete()

# Сортировка данных

# books.objects.order_by('name') # ASC
# books.objects.order_by('-name') # DESC

# books.objects.order_by('name').first() # Вывод первой записи
# books.objects.all().first()

# books.objects.order_by('name').last() # Вывод последней записи
# books.objects.all().last()

# books.objects.earliest('create_date') # Ранняя дата
# books.objects.latest('create_date') # Поздняя дата


# Books.objects.filter(category__title='Повесть')
# <QuerySet [<Books: Над пропастью во ржи>, <Books: Собачье сердце>, <Books: Тарас бульба>]>
# Books.objects.exclude(category__title='Повесть')
# <QuerySet [<Books: Алиса в Стране чудес>, <Books: Анна Каренина>, <Books: Маленький принц>, <Books: Фауст>]>
#
#
# Books.objects.filter(count_pages__gt=300).aggregate(Min('price'))
# {'price__min': 349.0}
# Books.objects.filter(count_pages__gt=300).aggregate(Max('price'))
# {'price__max': 843.0}
# Books.objects.filter(count_pages__gt=300).aggregate(Avg('price'))
# {'price__avg': 572.75}
#
#
# Books.objects.aggregate(Min('price'))
# {'price__min': 200.0}
# Books.objects.aggregate(Max('price'))
# {'price__max': 843.0}
# Books.objects.aggregate(Avg('price'))
# {'price__avg': 452.57142857142856}
# Books.objects.aggregate(Count('price'))
# {'price__count': 7}
# Books.objects.aggregate(Sum('price'))
# {'price__sum': 3168.0}
#
#
#
#
# book = Books.objects.filter(count_pages__gt=300)
# book.update(count_pages=F('count_pages')+1)
#
#
#
# Books.objects.filter(count_pages__gt=300).values('name','price')
#
# Books.objects.filter(count_pages__gt=300).values('name')
# <QuerySet [{'name': 'Алиса в Стране чудес'}, {'name': 'Анна Каренина'}, {'name': 'Собачье сердце'}, {'name': 'Фауст'}]>
# Books.objects.filter(count_pages__gt=300).values('price')
# <QuerySet [{'price': 843.0}, {'price': 693.0}, {'price': 406.0}, {'price': 349.0}]>
#
#
#
# Books.objects.filter(category__title='Роман')
# <QuerySet [<Books: Анна Каренина>, <Books: Над пропастью во ржи>]>
# Books.objects.filter(category__title='Повесть')
# <QuerySet [<Books: Над пропастью во ржи>, <Books: Собачье сердце>, <Books: Тарас бульба>]>
#
# Books.objects.filter(category__title__in=['Повесть','Роман'])
# <QuerySet [<Books: Анна Каренина>, <Books: Над пропастью во ржи>, <Books: Над пропастью во ржи>, <Books: Собачье сердце>, <Books: Тарас бульба>]>
#
# Books.objects.filter(Q(price__lt=300) | Q(price__gt=500))
# <QuerySet [<Books: Алиса в Стране чудес>, <Books: Анна Каренина>, <Books: Маленький принц>, <Books: Над пропастью во ржи>]>
#
# Books.objects.filter(price__gt=300, price__lt=500)
# <QuerySet [<Books: Собачье сердце>, <Books: Тарас бульба>, <Books: Фауст>]>
